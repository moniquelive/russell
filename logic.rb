require 'set'
require_relative 'substitution_list'
require_relative 'patches'
require_relative 'relations'
require_relative 'util'

module Logic
  class Solver
    include Relations

    # TODO: Still an overarching problem with:
    #       1. Passing logic variables into relations rather than values
    #          e.g. member currently requires a collection, and update_domain e.g.
    #          should gracefully handle both resolved and unresolved logic variables

    private
    attr_reader :possibilities, :rules

    public

    def initialize
      @possibilities = [IdentityStore.new]
      @rules = []
      self.instance_eval(&Proc.new) if block_given?
    end

    def more(&block)
      self.instance_eval(&block)
    end

    def assert(arg)
      arg.kind_of?(Proc) ? rule(arg) : fact(arg)
    end

    def fact(new_possibilities)
      add_possibilities(new_possibilities)
    end
    alias_method :facts, :fact

    def add_possibilities(new_possibilities)
      @possibilities = all(possibilities, new_possibilities)
    end

    def rule(proc)
      rules.push(proc)
    end

    def solve
      until possibilities.all?(&:solved)
        propagate_constraints
        break if possibilities.all?(&:solved)
        split_possibilities
      end
      propagate_constraints
    end

    def propagate_constraints
      loop do
        reset_changed_flags

        remaining_possibilities = possibilities.reject(&:solved)
        remaining_possibilities.each do |possibility|
          possibility.apply(rules)
        end
        possibilities.keep_if(&:valid)

        break unless something_changed
      end
    end

    def split_possibilities
      unsolved = possibilities.reject(&:solved)
      sorted_vars = unsolved.first.domains.to_a
                      .sort { |a, b| a[1].length <=> b[1].length }
      idx_smallest_domain = sorted_vars.find_index { |kv| kv[1].length > 1 }
      lvar = [sorted_vars[idx_smallest_domain][0]]
      possible_values = sorted_vars[idx_smallest_domain][1].to_a
      splits = lvar.product(possible_values).map { |combo| equal(*combo) }
      @possibilities = all(possibilities, any(*splits))
    end

    def results # reify
      possibilities.map(&:values)
    end

    def something_changed
      possibilities.any?(&:changed)
    end

    def reset_changed_flags
      possibilities.each(&:reset_changed)
    end
  end

  class IdentityStore
    include Relations

    def initialize
      @unifications = Logic::TriangularSubstitutionList.new
      @disequalities = []
      @domains = {}
      @valid = true
      @changed = true
      self.instance_eval(&Proc.new) if block_given?
    end

    protected
    attr_reader :unifications, :disequalities

    public
    attr_reader :valid, :solved, :domains, :changed

    def combine(other)
      other.domains.each { |lvar, domain| update_domain(lvar, domain) }
      other.disequalities.each { |dis| disjoin(*dis.to_a[0]) }
      other.unifications.each { |lvar, val| unify(lvar, val) }
      mark_changed
      self
    end

    def apply(rules)
      rules.each do |rule|
        result = self.instance_eval(&rule)
        combine(result[0]) if result
      end
    end

    def unify(left, right)
      changes = unifications.unify?(left, right)
      if changes == false
        invalidate
      else
        new_unifications = unifications.temp(changes)
        new_disequalities = disequalities.map do |dis|
          new_unifications.unify?(dis.keys, dis.values) || nil
        end
        new_disequalities.compact!
        invalidate if new_disequalities.any? { |dis| dis.empty? } # Unified something that was supposed to be prohibited...
        @unifications = new_unifications
        @disequalities = new_disequalities
        if changes.values[0].kind_of?(Numeric)
          update_domain(changes.to_a[0][0], [changes.to_a[0][1]])
        end
        mark_changed
      end
      self
    end

    def disjoin(left, right)
      if left.kind_of?(Symbol) && !right.kind_of?(Symbol) && domains.include?(left)
        narrow_domain(left, right)
      elsif right.kind_of?(Symbol) && !left.kind_of?(Symbol) && domains.include?(right)
        narrow_domain(right, left)
      else
        would_unify = unifications.unify?(left, right)
        if !would_unify # Perfect! Values already can't match.
        elsif would_unify.empty? # If would_unify returns no changes needed then
          invalidate #          the supposedly inequal values are already equated
        else
          disequalities.push(would_unify)
        end
      end
      mark_changed
      self
    end

    def update_domain(lvar, domain)
      if domains.include?(lvar)
        domains[lvar] = domains[lvar] & domain
      else
        domains[lvar] = Set.new(domain)
      end
      domains[lvar].delete_if do |val|
        disequalities.any? { |dis| dis[lvar] == val }
      end
      mark_changed
      check_domain_unified(lvar)
      check_domain_valid(lvar)
    end

    def narrow_domain(lvar, val)
      domains[lvar].delete?(val) && @changed = true
      check_domain_unified(lvar)
      check_domain_valid(lvar)
    end

    def invalidate
      @valid = false
    end

    def check_domain_valid(lvar)
      invalidate if domains[lvar].empty?
    end

    def check_domain_unified(lvar)
      set_from_domain(lvar) if domains[lvar].size == 1
    end

    def set_from_domain(lvar)
      domain = domains[lvar]
      return false if domain.size != 1
      unify(lvar, domain.to_a[0])
      mark_changed
    end

    def walk(val)
      @unifications.walk(val)
    end

    def values # AKA reify
      # TODO: return deduced values
      @unifications
    end

    def solved
      valid && domains.values.all? { |domain| domain.size == 1 }
    end

    def mark_changed
      @changed = true
    end

    def reset_changed
      @changed = false
    end
  end
end
