module Logic
  module Patches
    def Object.unify?(left, right, substitution_list)
      left == right ? substitution_list.class.new : false
    end

    def Array.unify?(left, right, substitution_list)
      return false if left.length != right.length
      final_result = substitution_list.class.new
      left.zip(right).each do |pair|
        result = substitution_list.unify?(*pair)
        if result == false
          final_result = false
          break
        else
          final_result.update(result)
        end
      end
      final_result
    end

    def Hash.unify?(left, right, substitution_list)
      return false if left.length != right.length
      final_result = substitution_list.class.new
      return final_result if left.empty? # (both, really, since length is the same)
      keys_result = substitution_list.unify?(left.keys, right.keys)
      return false if !keys_result
      values_result = substitution_list.unify?(left.values, right.values)
      return false if !values_result
      return false unless substitution_list.temp(keys_result).consistent?(values_result)
      final_result.update(keys_result).update(values_result)
    end

  end
end
